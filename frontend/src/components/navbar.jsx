import { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import useLogout from "../hooks/useLogout"

const Navbar = () => {
  const cart = useSelector(state => state.cart)
  const cartCount = cart.length
  const { logout } = useLogout()
  const user = JSON.parse(localStorage.getItem('user'))

  const handleLogout = () => {
    logout()
  }





  return (
    <nav className="fixed top-0 left-0 w-full bg-white z-50">
      <div className="mx-auto px-8">
        <div className="flex items-center justify-between h-16">
          <Link to="/" className="flex-shrink-0">
            <img
              src="/Logo.png"
              alt="Logo"
              className="h-10 w-10"
              style={{ width: "115px", height: "115px" }}
            />
          </Link>
          <div className="hidden md:block">
            <div className="ml-10 flex items-baseline space-x-4">
              <Link
                to="/"
                className="text-gray-800 hover:text-gray-600 px-3 py-2 rounded-md text-sm font-medium hover:underline"
              >
                Home
              </Link>
              
              <Link
                to="/about"
                className="text-gray-800 hover:text-gray-600 px-3 py-2 rounded-md text-sm font-medium hover:underline"
              >
                About
              </Link>
              <Link
                to="/contact"
                className="text-gray-800 hover:text-gray-600 px-3 py-2 rounded-md text-sm font-medium hover:underline"
              >
                Contact
              </Link>
            
              <div className="-mr-2 flex md:hidden">
          
          </div>
              <div>
                {user && <><Link
                  to="/products"
                  className="text-gray-800 hover:text-gray-600 px-3 py-2 rounded-md text-sm font-medium hover:underline"
                >
                  Products
                </Link>
                <Link
                  to="/Cart"
                  className="text-gray-800 hover:text-gray-600 px-3 py-2 rounded-md text-sm font-medium hover:underline"
                >
                  Cart  <span class="inline-flex items-center justify-center w-4 h-4 ml-2 text-xs font-semibold text-blue-800 bg-blue-200 rounded-full">{cartCount}
                </span>
                </Link>
                <button onClick={handleLogout} aria-label="logout" className=" text-base text-gray-800 focus:outline-none focus:ring-2 focus:ring-gray-800 hover:underline">Logout
                  </button>
                  </>

                  }
          </div>
              {!user && <>
                    <Link to="/signup" className=" text-base text-gray-800 focus:outline-none focus:ring-2 focus:ring-gray-800 hover:underline">
                      Signup
                    </Link>
                    <Link to="/login" className=" text-base text-gray-800 focus:outline-none focus:ring-2 focus:ring-gray-800 hover:underline">
                      Login
                    </Link>
                    </>
                 }
            </div>
            
        
        
                </div>
                </div>
              
            
              
                
                </div>
                </nav>
                
                );
                };
                
                export default Navbar;