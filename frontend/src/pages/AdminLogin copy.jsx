import React, { useState } from 'react';
import axios from 'axios';

const AdminLogin = () => {
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const credentials = {
      email: user,
      password: password
    };

    try {
      const response = await axios.post('/api/user', credentials);
      if (response.data.success) {
        // Redirect the user to the admin page
        window.location.href = '/admin-page';
      } else {
        // Handle invalid credentials or display error message
        console.log('Invalid 311263456456321345634563456');
      }
    } catch (error) {
      console.error('Login error:', error);
    }
  };

  return (
    <div className="flex items-center justify-center min-h-screen bg-gray-200">
      <div className="w-96">
        <h1 className="text-3xl text-center mb-8">Admin Login</h1>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Email"
            value={user}
            onChange={(e) => setUser(e.target.value)}
            className="w-full border border-gray-300 rounded py-2 px-4 mb-4"
          />
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className="w-full border border-gray-300 rounded py-2 px-4 mb-4"
          />
          <button
            type="submit"
            className="w-full bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default AdminLogin;
