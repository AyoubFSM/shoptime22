import { Link } from "react-router-dom";



const Home = () => {
  return (
    <>
    <div className="relative flex flex-col-reverse px-4 py-16 mx-auto lg:block lg:flex-col lg:py-32 xl:py-48 md:px-8 sm:max-w-xl md:max-w-full">
      <div className="z-0 flex justify-center h-full -mx-4 overflow-hidden lg:pt-24 lg:pb-16 lg:pr-8 xl:pr-0 lg:w-1/2 lg:absolute lg:justify-end lg:bottom-0 lg:left-0 lg:items-center">
        <img
          src="https://kitwind.io/assets/kometa/laptop.png"
          className="object-cover object-right w-full h-auto lg:w-auto lg:h-full"
          alt=""
        />
      </div>
      <div className="relative flex justify-end max-w-xl mx-auto xl:pr-32 lg:max-w-screen-xl">
        <div className="mb-16 lg:pr-5 lg:max-w-lg lg:mb-0">
          <div className="max-w-xl mb-6">
            <div>
              <p className="inline-block  py-px mb-4 text-xs font-semibold tracking-wider text-teal-900 uppercase rounded-full bg-teal-accent-400">
                Wellcom to <span className="text-blue-600">ShopiTime</span>                </p>
            </div>
            <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl sm:leading-none">
            It is better to win on hay trading than to lose on gold             
              
              <span className="inline-block text-deep-blue-accent-400">
                online
              </span>
            </h2>
            <p className="text-base text-gray-700 md:text-lg">
            ShopTime is an AI-based platform that helps integrate smart home devices, saving time and money. It offers voice-controlled smart speakers, security cameras, and appliance control via smartphones. ShopTime makes your home smarter and more efficient.
            </p>
          </div>
          <form>
            
            <div className="flex items-center mt-4">
            <Link to="/Products">  <button
                type="submit"
                className="inline-flex items-center justify-center h-12 px-6 mr-6 font-medium tracking-wide text-white transition duration-200 rounded shadow-md bg-blue-600 hover:bg-deep-blue-accent-700 focus:shadow-outline focus:outline-none"
              >
                Products
              </button></Link>
              <a
                href="/"
                aria-label=""
                className="inline-flex items-center font-semibold transition-colors duration-200 text-deep-blue-accent-400 hover:text-deep-blue-800"
              >
                Learn more
              </a>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
    <div class="grid grid-cols-2 row-gap-8 md:grid-cols-4">
      <div class="text-center md:border-r">
        <h6 class="text-4xl font-bold lg:text-5xl xl:text-6xl">144K</h6>
        <p class="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base">
          Producs
        </p>
      </div>
      <div class="text-center md:border-r">
        <h6 class="text-4xl font-bold lg:text-5xl xl:text-6xl">12.9K</h6>
        <p class="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base">
          label
        </p>
      </div>
      <div class="text-center md:border-r">
        <h6 class="text-4xl font-bold lg:text-5xl xl:text-6xl">48.3K</h6>
        <p class="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base">
          Users
        </p>
      </div>
      <div class="text-center">
        <h6 class="text-4xl font-bold lg:text-5xl xl:text-6xl">24.5K</h6>
        <p class="text-sm font-medium tracking-widest text-gray-800 uppercase lg:text-base">
          Cookies
        </p>
      </div>
    </div>
  </div>
  </>
  
    
  );
};
export default Home

  {/* {products.length > 0 &&
            products.map((p, i) => (
              <a
               
                className="group relative block overflow-hidden"
                key={i}
              >
                <img
                  src={p.image}
                  alt={p.title}
                  className="h-64 w-full object-cover transition duration-500 group-hover:scale-105 sm:h-72"
                />
                <div className="relative border border-gray-100 bg-white p-6">
                  <span className="whitespace-nowrap bg-yellow-400 px-3 py-1.5 text-xs font-medium">
                    {" "}
                    New{" "}
                  </span>

                  <h3 className="mt-auto text-lg font-medium text-gray-900">
                    {p.title}
                  </h3>

                  <p className="mt-1.5 text-sm text-gray-700">${p.price}</p>

                  <form className="mt-4">
                    {!addedProducts.includes(p.id) ? (
                      <button
                        onClick={() =>
                          handleAddProduct(p.id, p.title, p.price, p.image)
                        }
                        className="block w-full rounded bg-blue-600 p-4 text-white font-medium transition hover:scale-105"
                      >
                        Add to cart
                      </button>
                    ) : (
                      <button className="block w-full rounded bg-gray-400 p-4 text-sm font-medium transition" disabled>
                        Added to cart
                      </button>
                    )}
                  </form>
                </div>
              </a>
            ))} */}