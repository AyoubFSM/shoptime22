import React from 'react';

function About() {
  return (
<div className="flex flex-col min-h-screen text-center">
  <div className="py-8 lg:py-12  flex-grow">
    <div className="container mx-auto px-4 sm:px-6 lg:px-8 min-h-full">
      <h2 className="text-3xl leading-9  tracking-tight text-gray-800 sm:text-4xl sm:leading-10">
        About Us
      </h2>
      <div className="mt-6 prose prose-lg text-gray-500">
        <p>Our online store is committed to providing you with the best shopping experience possible. Shopping online has many benefits and advantages:</p>

        <ul className=" list-inside mt-4">
          <li className="mb-2">Convenience - shop from the comfort of your own home</li>
          <li className="mb-2">Variety - access to a wider range of products than physical stores</li>
          <li className="mb-2">Availability - online stores are open 24/7</li>
          <li className="mb-2">Saves Time - easily find what you're looking for and place your order</li>
          <li className="mb-2">Reviews and Feedback - make informed decisions about what to buy</li>
          <li className="mb-2">Easy Comparison - compare prices and products from different retailers</li>
          <li className="mb-2">Safe Payment Options - protect your financial information</li>
        </ul>
      </div>
    </div>
  </div>
  <div className='m-10'></div>
</div>

  );
}

export default About;
